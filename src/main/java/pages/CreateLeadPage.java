package pages;

import wrappers.Annotations;

public class CreateLeadPage extends Annotations {
	public CreateLeadPage EnterCompanyName(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
		return this;
	
}
	
	public CreateLeadPage EnterFirstName(String data) {
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
		return this;
	}
	
	public CreateLeadPage EnterLastName(String data) {

		driver.findElementById("createLeadForm_lastName").sendKeys(data);
		return this;
	}
		
		public Viewleads ClickCreateLeadButton() {
			
			driver.findElementByXPath("//input[@name='submitButton']").click();
			
			return new Viewleads();
		}
	
		
	}

