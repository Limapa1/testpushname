package pages;

import wrappers.Annotations;

public class MyHomePage extends Annotations {

	public CreateLeadPage ClickCreateLeadButton() {
			driver.findElementByLinkText("Create Lead").click();
			return new CreateLeadPage();
	
	
}
}